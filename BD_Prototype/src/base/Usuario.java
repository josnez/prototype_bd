package base;

public class Usuario implements Datos{
	
	private String nombre;
	private String apellido;
	private int documento;
	private int telefono;
	private String email;
	private int id;
	
	public String getNombre() {
		return nombre;
	}
	
	public String getApellido() {
		return apellido;
	}
	
	public int getDocumento() {
		return documento;
	}
	
	public int getTelefono() {
		return telefono;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public void setDocumento(int documento) {
		this.documento = documento;
	}
	
	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String toString() {
		
		return id+"\t"+nombre+"\t\t"+apellido+"\t\t"+documento+"\t\t"+telefono+"\t\t"+email;
	}
	
	@Override
	public Datos clonar() {
		
		Datos copia = new Usuario();
		copia.setNombre(this.nombre);
		copia.setApellido(this.apellido);
		copia.setDocumento(this.documento);
		copia.setTelefono(this.telefono);
		copia.setEmail(this.email);
		return copia;
	}
	
	
}
